package com.example.greed;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GreedApplication {

    public static void main(String[] args) {
        SpringApplication.run(GreedApplication.class, args);
    }

}
