package com.example.greed;

import java.util.Arrays;

public class Greed {

    public static final String SCORE_FOR = "Score for ";

    public static String greedy(int[] dice){

        return prefixAndArray(dice).concat(" must be 250:");


        }

    public static String prefixAndArray(int[] ints) {
        return SCORE_FOR.concat(Arrays.toString(ints).replace(" ",""));
    }
}
