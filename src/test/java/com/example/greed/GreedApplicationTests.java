package com.example.greed;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

@SpringBootTest
class GreedApplicationTests {

    @Test
    void contextLoads() {
    }

    @Test
    void givenPrefix_returnText() {
        String result = Greed.SCORE_FOR;
        assertEquals("Score for ", result);
    }

    @Test
    void givenArray_returnText() {
        String result = Greed.prefixAndArray(new int[]{5,1,3,4,1});
        assertEquals("Score for [5,1,3,4,1]", result);
    }
        @Test
        public void testExample() {
          //  int result = Greed.greedy(new int[]{1});
           // assertEquals("Score for [5,1,3,4,1] must be 250:", result );
//            assertEquals("Score for [1,1,1,3,1] must be 1100:", 1100);
//            assertEquals("Score for [2,4,4,5,4] must be 450:", 450);
    }

    @Test
    void myTest(){

        assertEquals("Score for [5,1,3,4,1] must be 250:",Greed.greedy(new int[]{5,1,3,4,1}));

    }
}
